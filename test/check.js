'use strict';

const rpcCall = require('kool-makerpccall');
const rpc = 'https://mainnet.infura.io/mew';

const tx = {
	to: '0x62d079e2795d500da6bdc6d03cb79ecd72a35e13',
	data: '0x0000000000000000000000000000000000001b84b1cb32787B0D64758d019317'
};

(async () => {
	console.log(await rpcCall(rpc, 'eth_call', [tx, 'latest']));
	console.log(Number(await rpcCall(rpc, 'eth_estimateGas', [ tx ])));
	console.log((await rpcCall(rpc, 'eth_getCode', ['0x0000000000001b84b1cb32787B0D64758d019317', 'latest'])).length);
})().catch((err) => console.error(err.stack));
