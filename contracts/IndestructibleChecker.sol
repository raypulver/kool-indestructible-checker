pragma solidity ^0.5.0;
contract IndestructibleChecker {
  function _isPotentiallyDestructible(
    address target
  ) internal view returns (bool potentiallyDestructible) {
    // Get the size of the target.
    uint256 size;
    assembly { size := extcodesize(target) }
    require(size > 0, "No code at target.");
    
    // Get the code at the target.
    bytes memory extcode = new bytes(size);
    uint256 dataStart;
    assembly { dataStart := add(extcode, 0x20) }
    assembly { extcodecopy(target, dataStart, 0, size) }
    
    // Look for any reachable, impermissible opcodes.
    bool reachable = true;
    uint256 op;
    for (uint256 i = dataStart; i < dataStart + size; i++) {
      assembly { op := shr(0xf8, mload(i)) }
      
      if (reachable) {
        if (
          op == 254 || // invalid
          op == 243 || // return
          op == 253 || // revert
          op == 86  || // jump
          op == 0      // stop
        ) {
          reachable = false;
          continue;
        }
        
        if (
          op == 242 || // callcode
          op == 244 || // delegatecall
          op == 255    // selfdestruct
        ) {
          return true; // potentially destructible!
        }
        
        if (op > 95 && op < 128) { // pushN
          i += (op - 95);
        }
      } else if (op == 91) { // jumpdest
        reachable = true;
      }
    }
  }
  
  function isPotentiallyDestructible(address target) external view returns (bool) {
      return _isPotentiallyDestructible(target);
  }
}
