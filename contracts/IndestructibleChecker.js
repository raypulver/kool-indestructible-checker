'use strict';

const emasm = require('emasm');
const makeConstructor = require('emasm/macros/make-constructor');
const makeJumpTable = require('emasm/macros/jump-table');

const incrementCounter = () => [
	'0x1',
	'add'
];

const reenterMainLoop = () => [
	'loop',
	'jump'
];

const loadOpcode = () => [
  'dup4', // freeptr, i, reachable, size, freeptr
	'dup2', // i, freeptr, i, reachable, size, freeptr
	'add', // freeptr + i, i, reachable, size, freeptr
	'mload',
	'0xf8',
	'shr' // [ op, i, reachable, size, freeptr ]
];

const shouldContinueParsing = () => [
	'dup3', // [ size, i, reachable, size, freeptr ]
	'dup2', // [ i, size, i, reachable, size, freeptr ]
	'lt' // [ continue, i, reachable, size, freeptr ]
];

const returnZero = () => [
	'0x20', // [ 0x20, i, reachable, size, freeptr ]
	'dup4', // [ size 0x20 reachable i size freeptr ]
	'dup6', 
	'add',
	'return'
];

const contract = emasm(makeConstructor([
	'bytes:jump-table:size', // [ table-size ]
	'bytes:jump-table:ptr', // [ table-ptr, table-size ]
	'0x0', // [ 0x0, table-ptr, table-size ]
	'codecopy', // [ ]
	'0x0', // [ 0x0 ]
	'calldataload', // [ target ]
	'dup1', // [ target, target ]
	'extcodesize', // [ size, target ]
	'dup1', // [ size, size, target ]
	'swap2', // [ target, size, size ]
	'swap1', // [ size, target, size ]
	'0x0', // [ 0, size, target, size ]
	'bytes:jump-table:size', // [ freeptr, 0, size, target, size ]
	'dup1', // [ freeptr, freeptr, 0, size, target, size ]
  'swap4', // [ target, freeptr, 0, size, freeptr, size ]
  'extcodecopy', // [ freeptr, size ]
	'swap1', // [ size, freeptr ]
	'0x1', // [ reachable, size, freeptr ]
  '0x0', // [ i, reachable, size, freeptr ]
	['loop', [
		shouldContinueParsing(),
		'action', 
		'jumpi',
		returnZero()
	]],
	['action', [ // i, reachable, size, freeptr
		loadOpcode(), // [ op, i, reachable, size, freeptr ]
		'0x20',
    'mul', 
		'mload',// [ jumpdest, i, reachable, size, freeptr ]
		'jump'
	]],
	['scan-for-jumpdest', [
		loadOpcode(),
		'0x5b', // 0x5b, op, i, reachable, size, freeptr
		'eq',
		'loop',
		'jumpi',
	]],
	['unreachable-loop', [
    incrementCounter(),
		shouldContinueParsing(),
		'scan-for-jumpdest',
		'jumpi',
		returnZero()
	]],
	['scan:invalid', 'scan:return', 'scan:revert', 'scan:jump', 'scan:stop'].map((label) => [
		label, [
		  'unreachable-loop',
		  'jump'
		]
	]),
	['scan:callcode', 'scan:delegatecall', 'scan:selfdestruct'].map((label) => [ label, [
		'0x1',
		'0x0',
		'mstore',
		'0x20',
		'0x0',
		'return'
	]]),
	Array.apply(null, { length: 32 }).map((_, i) => [
		'scan:push' + String(i + 1),
		[
			Number(i + 2),
			'add', // i, reachable, size, freeptr
			reenterMainLoop()
		]
	]),
	['scan:ignore', [
		incrementCounter(),
		reenterMainLoop()
	]],
	makeJumpTable('jump-table', Object.assign(Array.apply(null, { length: 256 }).fill(0), { 
		'0': 'scan:stop',
		'86': 'scan:jump',
		'242': 'scan:callcode',
		'243': 'scan:return',
		'244': 'scan:delegatecall',
		'253': 'scan:revert',
		'254': 'scan:invalid',
		'255': 'scan:selfdestruct'
	}, Array.apply(null, { length: 32 }).reduce((r, v, i) => {
	  r[i + 96] = 'scan:push' + String(i + 1);
	  return r;
	}, {})).map((v) => v || 'scan:ignore'))
]));

module.exports = contract;
